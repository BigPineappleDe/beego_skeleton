package controllers

import (
	"api.fenglide.com.cn/logs"
)

type ErrorController struct {
	BaseController
}

var errConfig = logs.LoggerConfig{}

func (c *ErrorController) Error401(msg string) {
	err := msg
	if msg == "" {
		err = "未经授权，请求要求验证身份"
	}
	errConfig.SetLog(err, 0)
	c.Error(err)
	return
}
func (c *ErrorController) Error403() {
	err := "服务器拒绝请求"
	errConfig.SetLog(err, 0)
	c.Error(err)
	return
}
func (c *ErrorController) Error404() {
	err := "很抱歉您访问的地址或者方法不存在"
	errConfig.SetLog(err, 0)
	c.Error(err)
	return
}

func (c *ErrorController) Error500() {
	err := "server error"
	errConfig.SetLog(err, 0)
	c.unknown(err)
	return
}
func (c *ErrorController) Error503() {
	err := "服务器目前无法使用（由于超载或停机维护）"
	errConfig.SetLog(err, 0)
	c.unknown(err)
	return
}

func (c *ErrorController) ErrorMsg() {
	err := "请携带令牌访问！"
	errConfig.SetLog(err, 0)
	c.Error(err)
	return
}

func (c *ErrorController) ErrorPost()  {
	err := "必须POST提交！"
	errConfig.SetLog(err, 0)
	c.Error(err)
	return
}
