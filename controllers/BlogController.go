/**
博客api
*/
package controllers

import (
	"api.fenglide.com.cn/common"
	"api.fenglide.com.cn/models"
	"encoding/json"
	"strconv"
)

type BlogController struct {
	BaseController
}

//获取文章数据
func (b *BlogController) Index() {
	req := make(map[string]interface{})
	data := b.Ctx.Input.RequestBody //在RequestBody中读取Json
	err := json.Unmarshal(data, &req)
	if err != nil {
		b.Error("请提交数据！")
	}
	//page判断是否存在
	_, pageBool := req["page"]
	if !pageBool {
		req["page"] = 0.0
	}
	//limit判断是否存在
	_, limitBool := req["limit"]
	if !limitBool {
		req["limit"] = 0.0
	}
	//id判断是否存在
	_, idBool := req["id"]
	if !idBool {
		req["id"] = "0"
	}
	if req["id"] != "0" {
		req["id"] = common.Decrypt(req["id"].(string)) //解密后传回去
	}
	//classId判断是否存在
	_, classIdBool := req["classId"]
	if !classIdBool {
		req["classId"] = "0"
	}
	if req["classId"] != "0" {
		req["classId"] = common.Decrypt(req["classId"].(string)) //解密后传回去
	}
	//LabelId判断是否存在
	_, labelIdBool := req["labelId"]
	if !labelIdBool {
		req["labelId"] = "0"
	}
	if req["labelId"] != "0" {
		req["labelId"] = common.Decrypt(req["labelId"].(string)) //解密后传回去
	}
	//type判断是否存在
	_, typeBool := req["type"]
	if !typeBool {
		req["type"] = ""
	}
	//like判断是否存在
	_, likeBool := req["like"]
	if !likeBool {
		req["like"] = ""
	}
	if req["like"] != "" {
		req["like"] = common.GetStr(req["like"].(string)) //过滤字符
	}
	//先转为对应类型再转为指定类型
	id, _ := strconv.Atoi(req["id"].(string))
	classId, _ := strconv.Atoi(req["classId"].(string))
	labelId, _ := strconv.Atoi(req["labelId"].(string))
	page := int(req["page"].(float64))
	limit := int(req["limit"].(float64))
	types := req["type"].(string)
	like := common.GetStr(req["like"].(string))
	//获取list数据
	list, pageSize := models.GetBlog(id, labelId, classId, page, limit, types, like)
	arr := map[string]interface{}{"list": list, "pageSize": pageSize}
	b.Success(arr)
}

//获取文章分类列表
func (b *BlogController) ClassList() {
	req := make(map[string]interface{})
	data := b.Ctx.Input.RequestBody //在RequestBody中读取Json
	_ = json.Unmarshal(data, &req)
	_, idBool := req["id"]
	if !idBool {
		req["id"] = "0"
	} else {
		req["id"] = common.Decrypt(req["id"].(string))
	}
	id, _ := strconv.Atoi(req["id"].(string))
	list := models.GetClassList(id)
	b.Success(list)
}

//获取标签列表
func (b *BlogController) LabelList() {
	req := make(map[string]interface{})
	data := b.Ctx.Input.RequestBody //在RequestBody中读取Json
	err := json.Unmarshal(data, &req)
	if err != nil {
		b.Error("请提交数据！")
	}
	_, idBool := req["id"]
	if !idBool {
		req["id"] = ""
	} else {
		req["id"] = common.Decrypt(req["id"].(string))
	}
	_, idsBool := req["ids"]
	if !idsBool {
		req["ids"] = 0.0
	} else {
		req["ids"] = common.Decrypt(req["ids"].(string))
	}
	id, _ := strconv.Atoi(req["id"].(string))
	ids := req["ids"].(string)
	list := models.GetLabelList(id, ids)
	b.Success(list)
}

//设置列表
func (b *BlogController) SettingList() {
	list := models.GetSetting()
	b.Success(list)
}
