package controllers

import (
	"github.com/astaxie/beego"
)

//公共控制器
type BaseController struct {
	beego.Controller
}

//输出json
type EchoJson struct {
	Code string      `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

var echoJson EchoJson
//成功
func (this *BaseController) Success(data interface{}) {
	echoJson.Code = beego.AppConfig.String("successCode")
	echoJson.Msg = beego.AppConfig.String("successMsg")
	echoJson.Data = data
	this.Data["json"] = echoJson
	this.ServeJSON()
	return
}

//失败
func (this *BaseController) Error(msg string) {
	echoJson.Data = nil
	if msg == "" {
		echoJson.Msg = beego.AppConfig.String("errorMsg")
	} else {
		echoJson.Msg = msg
	}
	echoJson.Code = beego.AppConfig.String("errorCode")
	this.Data["json"] = echoJson
	this.ServeJSON()
	return
}

//未知错误
func (this *BaseController) unknown(msg string) {
	echoJson.Data = nil
	if msg == "" {
		echoJson.Msg = beego.AppConfig.String("unknownMsg")
	} else {
		echoJson.Msg = msg
	}
	echoJson.Code = beego.AppConfig.String("unknownCode")
	this.Data["json"] = echoJson
	this.ServeJSON()
	return
}
