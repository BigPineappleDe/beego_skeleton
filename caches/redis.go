package caches

import (
	"github.com/garyburd/redigo/redis"
)

var pool *redis.Pool

func Init()  {
	pool = &redis.Pool{
		MaxIdle:16,
		MaxActive:0,
		IdleTimeout:300,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", "localhost:6379")
		},
	}
	/*c := pool.Get()
	defer c.Close()

	_, err := c.Do("set", "abc", 100)
	if err != nil {
		fmt.Println("c.Do err ", err)
		return
	}

	r, err := redis.Int(c.Do("get", "abc"))
	if err != nil {
		fmt.Println("get abc failed",err)
		return
	}
	fmt.Println(r)
	fmt.Printf("r type is %T", r)
	pool.Close()*/
}