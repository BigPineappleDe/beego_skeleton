package routers

import (
	"api.fenglide.com.cn/controllers"
	"github.com/astaxie/beego"
)

func init() {

	ns := beego.NewNamespace("/error",
		beego.NSRouter("/err", &controllers.ErrorController{}, "*:ErrorMsg"),
		beego.NSRouter("/post", &controllers.ErrorController{}, "*:ErrorPost"),
	)
	//博客路由
	nsApi := beego.NewNamespace("/api",
		beego.NSNamespace("/blog",
			beego.NSRouter("/list", &controllers.BlogController{}, "*:Index"),//文章列表
			beego.NSRouter("/class", &controllers.BlogController{}, "*:ClassList"),//分类列表
			beego.NSRouter("/label", &controllers.BlogController{}, "*:LabelList"),//标签列表
			beego.NSRouter("/setting", &controllers.BlogController{}, "*:SettingList"),//设置列表
		),
	)
	beego.AddNamespace(ns, nsApi)
}
