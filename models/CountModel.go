package models

import "github.com/astaxie/beego/orm"

/**
统计表
 */
type CountModel struct {
	Id int
}

func (c *CountModel) TableName() string {
	return TableName("log_count")
}

func init(){
	orm.RegisterModel(new(CountModel))
}