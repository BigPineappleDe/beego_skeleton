package models

import (
	"api.fenglide.com.cn/common"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"strconv"
)

/**
分类模型
*/
type ClassModel struct {
	Id         int    `orm:"column(class_id);pk" json:"id"`
	ClassName  string `orm:"column(class_name)" json:"className"`
	ClassKey   string `orm:"column(class_key)" json:"classKey"`
	AddTime    int    `orm:"column(class_add_time)" json:"classAddTime"`
	ClassState int    `orm:"column(class_state)" json:"classState"`
	AdminId    int    `orm:"column(admin_id)" json:"adminId"`
	ClassIcon  string `orm:"column(class_icon)" json:"classIcon"`
}

func (c *ClassModel) TableName() string {
	return TableName("class")
}
func init() {
	orm.RegisterModel(new(ClassModel))
}

//获取class列表
func GetClassList(id int) interface{} {
	o := orm.NewOrm().QueryTable(new(ClassModel).TableName()).Filter("ClassState", 0)
	var list []orm.Params
	if id != 0 {
		_, _ = o.Filter("Id", id).Values(&list, "Id", "ClassName", "ClassKey", "ClassIcon")
		list[0]["Id"] = common.Encryption(strconv.FormatInt(list[0]["Id"].(int64), 10))
		list[0]["ClassIcon"] = "https://" + beego.AppConfig.String("resUrl") + "/" + list[0]["ClassIcon"].(string)
		return list[0]
	}
	o = o.OrderBy("-Id")
	_, _ = o.Values(&list, "Id", "ClassName", "ClassKey", "ClassIcon")
	for key := range list {
		list[key]["Id"] = common.Encryption(strconv.FormatInt(list[key]["Id"].(int64), 10))
		list[key]["ClassIcon"] = "https://" + beego.AppConfig.String("resUrl") + "/" + list[key]["ClassIcon"].(string)
	}
	return list
}
