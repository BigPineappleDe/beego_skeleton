package models

import (
	"api.fenglide.com.cn/common"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"math"
	"strconv"
	"strings"
	"time"
)

/**
博客模型
*/
type BlogModel struct {
	Id       int    `orm:"column(article_id);pk" json:"id"`         //id
	Img      string `orm:"column(article_img)" json:"img"`          //图片
	Title    string `orm:"column(article_title)" json:"title"`      //标题
	Content  string `orm:"column(article_content)" json:"content"`  //内容
	AddTime  int    `orm:"column(article_add_time)" json:"addTime"` //更新日期
	AdminId  int    `orm:"column(admin_id)" json:"adminId"`         //更新员
	ClassId  int    `orm:"column(class_id)" json:"classId"`         //类ID
	LabelIds string `orm:"column(label_ids)" json:"labelIds"`       //相关标签ID
	State    int    `orm:"column(article_state)" json:"state"`      //状态 0启用
	Source   string `orm:"column(article_source)" json:"source"`    //文章来源 0本站
	LabelId  int    `orm:"column(label_id)" json:"labelId"`         //标签id
	Type     int    `orm:"column(article_type)" json:"type"`        //类型 1普通 2banner 3推荐
	Mode     int    `orm:"column(article_mode)" json:"mode"`        //写入方式 0手动录入
}

func (b *BlogModel) TableName() string {
	return TableName("article")
}

func init() {
	orm.RegisterModel(new(BlogModel))
}

//获取博客文章列表
func GetBlog(id int, labelId int, classId int, page int, limit int, lx string, like string) (interface{}, int) {
	var list []orm.Params
	var listCount []orm.Params
	where := " where 1 and article_state = 0 "
	limits := " "
	nums := " (select count(0) num from " + new(CommentMode).TableName() + " where q.article_id=article_id ) CommentNum, (select count(0) showCount from " + new(CountModel).TableName() + " where q.article_id=article_id) ShowCount,"
	p := nums + " q.article_id Id,q.article_title Title,q.article_content Content,q.article_add_time AddTime,q.article_mode Mode,q.article_img Img,q.label_id LabelId,q.label_ids LabelIds,q.article_source Source "
	pCount := " count(0) num "
	orderBy := " order by article_id desc "
	if id != 0 {
		where += " and article_id = " + strconv.Itoa(id)
	}
	if labelId != 0 {
		where += " and label_id = " + strconv.Itoa(labelId)
		orderBy = " order by rand() "
	}
	if classId != 0 {
		where += " and class_id = " + strconv.Itoa(classId)
	}
	if lx == "banner" {
		where += " and article_type=2 "
		orderBy = " order by rand() "
	}
	if like != "" {
		where += " and article_title like '%" + like + "%' "
	}
	if page == 0 && limit != 0 {
		limits = " limit " + strconv.Itoa(limit)
	}
	if page != 0 && limit != 0 {
		limits = " limit " + strconv.Itoa((page-1)*limit) + "," + strconv.Itoa(limit)
	}
	sql := "select " + p + " from " + new(BlogModel).TableName() + " q " + where + orderBy + limits
	sqlCount := "select " + pCount + " from " + new(BlogModel).TableName() + where
	r := orm.NewOrm().Raw(sql)
	_, _ = r.Values(&list)
	c := orm.NewOrm().Raw(sqlCount)
	_, _ = c.Values(&listCount)
	pageSize := limit
	if page != 0 {
		iListCount, _ := strconv.ParseInt(listCount[0]["num"].(string), 10, 64)
		pageSize = int(math.Ceil(float64(iListCount) / float64(limit))) //总页数
	}
	for key := range list {
		if id == 0 {
			//剥离html标签并指定长度数据
			list[key]["Content"] = common.ShowSubstr(common.TrimHtml(list[key]["Content"].(string)), 100)
		}
		//判断是否为本站文章 如果为本站文章则添加图片地址
		iMode, _ := strconv.ParseInt(list[key]["Mode"].(string), 10, 64)
		if iMode < 1 {
			list[key]["Img"] = "https://" + beego.AppConfig.String("resUrl") + "/" + list[key]["Img"].(string)
			list[key]["Content"] = strings.Replace(list[key]["Content"].(string), "<img src=\"", "<img src=\"https://"+beego.AppConfig.String("resUrl")+"/", -1)
		} else {
			list[key]["Content"] = list[key]["Content"].(string) + "<br><strong><i><p>" + list[key]["Source"].(string) + "</p></i></strong>"
		}

		list[key]["Id"] = common.Encryption(list[key]["Id"].(string))
		list[key]["LabelId"] = common.Encryption(list[key]["LabelId"].(string))
		list[key]["LabelIds"] = common.Encryption(list[key]["LabelIds"].(string))
		iAddtime, _ := strconv.ParseInt(list[key]["AddTime"].(string), 10, 64)
		list[key]["AddTime"] = time.Unix(iAddtime, 0).Format("2006-01-02")
	}

	if id != 0 {
		return list[0], pageSize
	}
	return list, pageSize
}
