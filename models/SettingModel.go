package models

import "github.com/astaxie/beego/orm"

/**
配置
 */
type SettingModel struct {
	Id int `orm:"column(setting_id);pk"`
	Key string `orm:"column(setting_key)"`
	State int `orm:"column(setting_state)"`
	AddTime int `orm:"column(setting_add_time)"`
	AdminId int `orm:"column(admin_id)"`
	Name string `orm:"column(setting_name)"`
}

func (s *SettingModel) TableName() string {
	return TableName("setting")
}

func init()  {
	orm.RegisterModel(new(SettingModel))
}

func GetSetting() interface{} {
	var list []orm.Params
	_, _ = orm.NewOrm().QueryTable(new(SettingModel).TableName()).Values(&list,"Key","State")
	return list
}


