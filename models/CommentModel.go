package models

import "github.com/astaxie/beego/orm"

/**
评论表
 */
type CommentMode struct {
	Id int
}

func (c *CommentMode) TableName() string {
	return TableName("comment")
}

func init(){
	orm.RegisterModel(new(CommentMode))
}