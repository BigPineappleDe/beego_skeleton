package models

import (
	"github.com/astaxie/beego/orm"
	"strings"
)

/**
博客标签
*/
type LabelModel struct {
	Id         int    `orm:"column(label_id);pk"`
	LabelName  string `orm:"column(label_name)"`
	LabelKey   string `orm:"column(label_key)"`
	AddTime    int    `orm:"column(label_add_time)"`
	AdminId    int    `orm:"column(admin_id)"`
	LabelState int    `orm:"column(label_state)"`
	ClassId    int    `orm:"column(class_id)"`
}

func (l *LabelModel) TableName() string {
	return TableName("label")
}

func init() {
	orm.RegisterModel(new(LabelModel))
}

//获取标签数据
func GetLabelList(id int, ids string) interface{} {
	o := orm.NewOrm().QueryTable(new(LabelModel).TableName())
	var list []orm.Params
	if id != 0 {

	}
	if ids != "" {
		idsArr := strings.Split(ids, `,`)
		i := len(idsArr)
		if i > 1 {
			o = o.Filter("Id__in", idsArr)
		} else {
			o = o.Filter("Id", ids)
		}
	}
	_, _ = o.Values(&list, "Id", "LabelName")
	return list
}
