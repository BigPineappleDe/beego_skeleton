package main

import (
	"api.fenglide.com.cn/controllers"
	"api.fenglide.com.cn/middlewares"
	"api.fenglide.com.cn/models"
	_ "api.fenglide.com.cn/routers"
	"github.com/astaxie/beego"
)

func init(){
	models.Init()
	//过滤器
	beego.InsertFilter("/*", beego.BeforeRouter, middlewares.Before) //index
	//异常处理
	beego.ErrorController(&controllers.ErrorController{})
}

func main() {
	beego.Run()
}

